package javaSpring1.model;

import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Configuration
@PropertySource("javaSpring1.resources.beanB.properties")
public class BeanB implements java.io.Serializable, BeanPostProcessor,
  Validator {
  private String name;
  private int value;
  public BeanB() { }
  public BeanB(String name, int value) {
    this.name = name;
    this.value = value;
  }
  @Override
  public String toString() {
    return "POJO2{" + "name='" + name + '\'' + ", value=" + value + '}';
  }

  public boolean supports(Class<?> aClass) {
    return false;
  }

  public void validate(Object o, Errors errors) {

  }
  public void onInit() {
    System.out.println("BeanB initiated");
  }
  public void onDestroy() {
    System.out.println("BeanB destroyed");
  }
}
