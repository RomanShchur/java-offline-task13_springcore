package javaSpring1.model;

import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanE implements java.io.Serializable, BeanPostProcessor,
  Validator {
  private String name;
  private int value;
  public BeanE() {  }
  public BeanE(String name, int value) {
    this.name = name;
    this.value = value;
  }
  @Override
  public String toString() {
    return "BeanE{" + "name='" + name + '\'' + ", value=" + value + '}';
  }
  @PostConstruct
  public void beanEpostConstruct() {
    System.out.println("BeanE constructed");
  }
  @PreDestroy
  public void beanEpreDestroy() {
    System.out.println("BeanE being destroyed");
  }

  public boolean supports(Class<?> aClass) {
    return false;
  }

  public void validate(Object o, Errors errors) {

  }
}
