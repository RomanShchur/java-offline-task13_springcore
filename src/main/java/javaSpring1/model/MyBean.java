package javaSpring1.model;

import org.springframework.beans.factory.config.BeanPostProcessor;

public class MyBean implements java.io.Serializable, BeanPostProcessor {
  private String name;
  private int value;
  public MyBean() {}
  public MyBean(String name, int value) {
    this.name = name;
    this.value = value;
  }

  @Override
  public String toString() {
    return "{" + "name='" + name + '\'' + ", value=" + value + '}';
  }
  public Object postProcessBeforeInitialization(Object bean, String beanName) {
    System.out.println("postProcessBeforeInitialization()");
    System.out.println(" >>bean=" + bean + " beanName=" + beanName);
    return bean;
  }
  public Object postProcessAfterInitialization(Object bean, String beanName) {
    System.out.println("postProcessAfterInitialization()");
    System.out.println(" >>bean=" + bean + " beanName=" + beanName);
    return bean;
  }
}
