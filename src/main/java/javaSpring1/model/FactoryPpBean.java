package javaSpring1.model;

import org.springframework.beans.BeansException;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.PropertyValue;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

public class FactoryPpBean implements BeanFactoryPostProcessor {
  public void postProcessBeanFactory(
    ConfigurableListableBeanFactory beanFactory)
    throws BeansException {
    System.out.println("postProcessBeanFactory is working");
    BeanDefinition bd = beanFactory.getBeanDefinition("BeanB");
    if(bd.hasPropertyValues()) {
      MutablePropertyValues pvs = bd.getPropertyValues();
      PropertyValue[] pvArray = pvs.getPropertyValues();
      for (PropertyValue pv : pvArray) {
//        System.out.println("pv -- " + pv.getName());
        if (pv.getName().equals("name")) {
          pvs.add(pv.getName(), "NewName");
          System.out.println("BeanB name is now NewName");
        }
      }
    }
  }
}
