package javaSpring1.model;

import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Configuration
@Lazy
public class BeanF implements java.io.Serializable, BeanPostProcessor,
  Validator {
  private String name;
  private int value;
  public BeanF() {
    System.out.println("BeanF Constructor");
  }
  public BeanF(String name, int value) {
    this.name = name;
    this.value = value;
  }
  @Override
  public String toString() {
    return "BeanE{" + "name='" + name + '\'' + ", value=" + value + '}';
  }

  public boolean supports(Class<?> aClass) {
    return false;
  }

  public void validate(Object o, Errors errors) {
  }
}
