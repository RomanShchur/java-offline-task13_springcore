package javaSpring1.model;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class BeanA implements java.io.Serializable, BeanPostProcessor,
  InitializingBean, DisposableBean, Validator {
  private String name;
  private int value;
  public BeanA() {
    this.name = "BeanA";
  }
  public BeanA(String name, int value) {
    this.name = "BeanA";
    this.value = value;
  }
  @Override
  public String toString() {
    return "BeanA{" + "name='" + name + '\'' + ", value=" + value + '}';
  }

  public void afterPropertiesSet() throws Exception {
    System.out.println("BeanA props set");
  }

  public void destroy() throws Exception {
    System.out.println("BeanA destroyed");
  }

  public boolean supports(Class<?> aClass) {
    return false;
  }

  public void validate(Object o, Errors errors) {

  }

}
