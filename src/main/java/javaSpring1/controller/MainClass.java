package javaSpring1.controller;

import javaSpring1.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

public class MainClass {
  public static void main(String[] args) {
    AnnotationConfigApplicationContext acac =
      new AnnotationConfigApplicationContext(BeansDeclarator.class);
    BeanB beanB = acac.getBean(BeanB.class);
    BeanC beanC = acac.getBean(BeanC.class);
    BeanD beanD = acac.getBean(BeanD.class);
    Object beanA = acac.getBean("BeanA");
    BeanE beanE = acac.getBean(BeanE.class);
    BeanF beanF = acac.getBean(BeanF.class);
    FactoryPpBean fpb = new FactoryPpBean();
    System.out.println("===== Declared Beans =====");
    String[] singletonNames = acac.getDefaultListableBeanFactory().getSingletonNames();
    for (String singleton : singletonNames) {
      System.out.println(singleton);
    }
  }
}
