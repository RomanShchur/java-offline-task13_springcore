package javaSpring1.controller;

import javaSpring1.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;

@Configuration
@Import(Config1.class)
public class BeansDeclarator {
  @Bean
  @DependsOn (value = { "BeanB" ,"BeanC", "BeanD"})
  @Autowired
  @Qualifier ("BeanA")
  public BeanA BeanA(){
    return new BeanA();
  }
  @Bean
  @DependsOn (value = {"BeanC", "BeanB"})
  public BeanA BeanA1(){
    return new BeanA();
  }
  @Bean
  @DependsOn (value = {"BeanD", "BeanB"})
  public BeanA BeanA2(){
    return new BeanA();
  }
  @Bean
  @DependsOn (value = {"BeanC", "BeanD"})
  public BeanA BeanA3(){
    return new BeanA();
  }
  @Bean (initMethod="onInit", destroyMethod = "onDestroy")
  @DependsOn (value = {"BeanD"})
  public BeanB BeanB(){
    return new BeanB();
  }
  @Bean (initMethod="onInit", destroyMethod = "onDestroy")
  @DependsOn (value = {"BeanB"})
  public BeanC BeanC(){
    return new BeanC();
  }
  @Bean (initMethod="onInit", destroyMethod = "onDestroy")
  public BeanD BeanD(){
    return new BeanD();
  }
  @Bean
  public BeanE BeanE(){
    return new BeanE();
  }
  @Bean
  public BeanF BeanF(){
    return new BeanF();
  }
}
